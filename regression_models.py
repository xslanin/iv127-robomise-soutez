from robomission import *
from xgboost import XGBRegressor
from sklearn.ensemble import AdaBoostRegressor, BaggingRegressor
from sklearn.linear_model import BayesianRidge, HuberRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

class LinearRegressionModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        cols = solved_attempts.columns
        X = solved_attempts.drop(["id", "student",], axis=1)
        
        #X = train_solved.drop(["start", "item_solution"], axis=1)

        #s = StandardScaler()
        #X_std = s.fit_transform(X)
        #pca = PCA(n_components=len(X.columns)).fit_transform(X_std)
        
        linreg = LinearRegression().fit(X, time)
        self.pparams = {
            'linreg': linreg,
            'item_mean_times': item_mean_times,
            'cols': cols #,
            #'scaler': s
        }
    
    def predict(self, attempt):
        linreg = self.pparams['linreg']
        item_mean_times = self.pparams['item_mean_times']
        #scaler = self.pparams['scaler']
        X_test = attempt.drop(["id", "student"])
        #print(X_test)
        #print(attempt)
        #X_std = scaler.inverse_transform(X_test)
        pred = linreg.predict([X_test])[0]
        item = item_mean_times.get(attempt["item"])

        return pred

    def debug(self):
        return self.pparams['linreg'], self.pparams['cols']

class LinearRegressionModel_(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        cols = solved_attempts.columns
        X = solved_attempts.drop(["id", "student"], axis=1)
        
        #X = train_solved.drop(["start", "item_solution"], axis=1)

        #s = StandardScaler()
        #X_std = s.fit_transform(X)
        #pca = PCA(n_components=len(X.columns)).fit_transform(X_std)
        
        linreg = LinearRegression().fit(X, time)
        self.pparams = {
            'linreg': linreg,
            'item_mean_times': item_mean_times,
            'cols': cols,
            'seen_students_in_test': [] #,
            #'scaler': s
        }
    
    def predict(self, attempt):
        linreg = self.pparams['linreg']
        item_mean_times = self.pparams['item_mean_times']
        worse_tasks_to_start_with = [2, 3, 10, 12, 13, 14, 15, 22, 26, 29, 31, 33, 36, 38, 42, 44, 48, 49, 64, 67, 78, 83, 86]
        handicap = 1
        if not attempt['student'] in self.pparams['seen_students_in_test']:
            if attempt['item'] in worse_tasks_to_start_with:
                handicap = 1.2
            self.pparams['seen_students_in_test'].append(attempt['student'])

        X_test = attempt.drop(["id", "student"])
        #print(X_test)
        #print(attempt)
        #X_std = scaler.inverse_transform(X_test)
        pred = linreg.predict([X_test])[0]
        item = item_mean_times.get(attempt["item"])

        return pred*handicap

    def debug(self):
        return self.pparams['linreg'], self.pparams['cols']

class LinearRegressionModelOnlySeen(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        X = solved_attempts.drop(["id", "student"], axis=1)
        linreg = LinearRegression().fit(X, time)
        #linreg = Ridge().fit(solved_attempts, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'linreg': linreg,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf
        }
    
    def predict(self, attempt):
        linreg = self.pparams['linreg']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']

        X_test = attempt.drop(["id", "student"])
        #print(attempt)
        pred = linreg.predict([X_test])[0]

        item = item_mean_times.get(attempt["item"])
        if attempt["student"] in student_perf:
            return pred
        return item

class RidgeRegressionModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        X = solved_attempts.drop(["id", "student"], axis=1)
        
        ridge_params = {
            'alpha': [1, 2, 5, 50, 100], 
            'solver': ['auto', 'svd', 'cholesky', 'lsqr'], 
            'max_iter': [1000, 10000, 100000]}
        ridge = GridSearchCV(Ridge(), param_grid=ridge_params).fit(X, time)

        #ridge = Ridge().fit(solved_attempts, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'ridge': ridge,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf
        }
    
    def predict(self, attempt):
        ridge = self.pparams['ridge']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        X = attempt.drop(["id", "student"])

        pred = ridge.predict([X])[0]
        #item = item_mean_times.get(attempt["item"])
        return pred

    def debug(self):
        return self.pparams['ridge']

class RidgeRegressionModelOnlySeen(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        '''
        ridge_params = {
            'alpha': [1, 2, 5, 50, 100], 
            'solver': ['auto', 'svd', 'cholesky', 'lsqr'], 
            'max_iter': [1000, 10000, 100000]}
        ridge = GridSearchCV(Ridge(), param_grid=ridge_params).fit(solved_attempts, time)
        '''
        ridge = Ridge().fit(solved_attempts, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'ridge': ridge,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf
        }
    
    def predict(self, attempt):
        ridge = self.pparams['ridge']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        #print(attempt)
        pred = ridge.predict([attempt])[0]
        item = item_mean_times.get(attempt["item"])
        if attempt["student"] in student_perf:
            if pred > item:
                return pred
        return item

    def debug(self):
        return self.pparams['ridge']

class LassoRegressionModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        X = solved_attempts.drop(["id", "student"], axis=1)

        lasso_params = {
            'alpha': [0.001, 0.01, 0.05, 0.1], 
            'max_iter': [1000]}
        lasso = GridSearchCV(Lasso(), param_grid=lasso_params).fit(X, time)

        
        cols = X.columns
        #lasso = Lasso(alpha=0.01).fit(solved_attempts, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'lasso': lasso,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf,
            'cols': cols
        }
    
    def predict(self, attempt):
        lasso = self.pparams['lasso']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        X = attempt.drop(["id", "student"])

        pred = lasso.predict([X])[0]
        item = item_mean_times.get(attempt["item"])
        #if attempt["student"] in student_perf:
        #if pred > item:
        return pred
       
    def debug(self):
        return self.pparams['lasso'], self.pparams['cols']

class LassoRegressionModelOnlySeen(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        '''
        lasso_params = {
            'alpha': [0.001, 0.01, 0.05, 0.1], 
            'max_iter': [1000]}
        lasso = GridSearchCV(Lasso(), param_grid=lasso_params).fit(solved_attempts, time)
        '''
        cols = solved_attempts.columns
        lasso = Lasso(alpha=0.01).fit(solved_attempts, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'lasso': lasso,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf,
            'cols': cols
        }
    
    def predict(self, attempt):
        lasso = self.pparams['lasso']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        #print(attempt)
        pred = lasso.predict([attempt])[0]
        item = item_mean_times.get(attempt["item"])
        #print(pred - item)
        if attempt["student"] in student_perf:
            if pred > item:
                return pred
        return item

    def debug(self):
        return self.pparams['lasso'], self.pparams['cols']

class ElasticNetModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        X = solved_attempts.drop(["id", "student"], axis=1)

        lasso_params = {
            'alpha': [0.001, 0.01, 0.05, 0.1], 
            'max_iter': [1000]}
        lasso = GridSearchCV(ElasticNet(), param_grid=lasso_params).fit(X, time)
        
        cols = X.columns
        #lasso = ElasticNet().fit(solved_attempts, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'lasso': lasso,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf,
            'cols': cols
        }
    
    def predict(self, attempt):
        lasso = self.pparams['lasso']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        X = attempt.drop(["id", "student"])

        pred = lasso.predict([X])[0]
        item = item_mean_times.get(attempt["item"])
        #if attempt["student"] in student_perf:
        #if abs(pred - item) > 1:
        #    print(attempt)
        #    print(pred)
        #    print(item)
        return pred
       
    def debug(self):
        return self.pparams['lasso'], self.pparams['cols']

class ElasticNetModelOnlySeen(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        '''
        lasso_params = {
            'alpha': [0.001, 0.01, 0.05, 0.1], 
            'max_iter': [1000]}
        lasso = GridSearchCV(Lasso(), param_grid=lasso_params).fit(solved_attempts, time)
        '''
        
        cols = solved_attempts.columns
        lasso = ElasticNet().fit(solved_attempts, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'lasso': lasso,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf,
            'cols': cols
        }
    
    def predict(self, attempt):
        lasso = self.pparams['lasso']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        #print(attempt)
        pred = lasso.predict([attempt])[0]
        item = item_mean_times.get(attempt["item"])
        if attempt["student"] in student_perf:
            if pred > item:
                return pred
       
    def debug(self):
        return self.pparams['lasso'], self.pparams['cols']

class XGBoostRegressionModel(StudentModel): #0.894
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        '''
        lasso_params = {
            'alpha': [0.001, 0.01, 0.05, 0.1], 
            'max_iter': [1000]}
        lasso = GridSearchCV(Lasso(), param_grid=lasso_params).fit(solved_attempts, time)
        '''
        X = solved_attempts.drop(["id", "student"], axis=1)

        cols = X.columns
        lasso = XGBRegressor(n_estimators=100, learning_rate=0.05).fit(X, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'lasso': lasso,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf,
            'cols': cols
        }
    
    def predict(self, attempt):
        lasso = self.pparams['lasso']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        attempt = attempt.drop(["id", "student"])
        print(attempt)
        a = attempt.to_frame().T
        a.index.name = "id"
        #a = a.set_index("id")
        a = a.astype('bool')
        a = a.astype({"item": "int64", "item_mean_time": "float64"})
        a = a.astype({"item_order": "int8"})
        a = a.astype({"startDay": "int64", "knowsColor": "float64", "knowsLoop": "float64", "knowsPosition": "float64"})
        a = a.astype({'startHour': 'int64'})

        '''
                a = a.astype('bool')
                a = a.astype({'student': 'int64', "event_order": 'int64', "item": "int64"})
                a = a.astype({'item': 'int8', "item_setting": 'int8', "item_solution": "int8"})
                a = a.astype({'startYear': 'int64', "startMonth": 'int64', "startWeek": "int64", "startDay": "int64", "startDayofweek": "int64", "startDayofyear": "int64"})
                a = a.astype({'startHour': 'int64', "startMinute": 'int64', "startSecond": "int64", "startElapsed": "int64"})

        '''

        pred = lasso.predict(a)
        item = item_mean_times.get(attempt["item"])
        return pred

    def debug(self):
        return self.pparams['lasso'], self.pparams['cols']

class RandomForestModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        X = solved_attempts.drop(["id", "student"], axis=1)
        '''
        lasso_params = {
            'alpha': [0.001, 0.01, 0.05, 0.1], 
            'max_iter': [1000]}
        lasso = GridSearchCV(ElasticNet(), param_grid=lasso_params).fit(X, time)
        '''
        cols = X.columns
        lasso = RandomForestRegressor(n_estimators =10, n_jobs=-1).fit(X, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'lasso': lasso,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf,
            'cols': cols
        }
    
    def predict(self, attempt):
        lasso = self.pparams['lasso']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        X = attempt.drop(["id", "student"])

        pred = lasso.predict([X])[0]
        item = item_mean_times.get(attempt["item"])
        return pred
       
    def debug(self):
        return self.pparams['lasso'], self.pparams['cols']


class AdaBoostRegressorModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        X = solved_attempts.drop(["id", "student"], axis=1)
        '''
        lasso_params = {
            'alpha': [0.001, 0.01, 0.05, 0.1], 
            'max_iter': [1000]}
        lasso = GridSearchCV(ElasticNet(), param_grid=lasso_params).fit(X, time)
        '''
        cols = X.columns
        lasso = AdaBoostRegressor().fit(X, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'lasso': lasso,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf,
            'cols': cols
        }
    
    def predict(self, attempt):
        lasso = self.pparams['lasso']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        X = attempt.drop(["id", "student"])

        pred = lasso.predict([X])[0]
        item = item_mean_times.get(attempt["item"])
        #if attempt["student"] in student_perf:
        #if abs(pred - item) > 1:
        #    print(attempt)
        #    print(pred)
        #    print(item)
        return pred
       
    def debug(self):
        return self.pparams['lasso'], self.pparams['cols']

class HuberRegressorModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        X = solved_attempts.drop(["id", "student"], axis=1)
        '''
        lasso_params = {
            'alpha': [0.001, 0.01, 0.05, 0.1], 
            'max_iter': [1000]}
        lasso = GridSearchCV(ElasticNet(), param_grid=lasso_params).fit(X, time)
        '''
        cols = X.columns
        lasso = HuberRegressor().fit(X, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'lasso': lasso,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf,
            'cols': cols
        }
    
    def predict(self, attempt):
        lasso = self.pparams['lasso']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        X = attempt.drop(["id", "student"])

        pred = lasso.predict([X])[0]
        item = item_mean_times.get(attempt["item"])
        #if attempt["student"] in student_perf:
        #if abs(pred - item) > 1:
        #    print(attempt)
        #    print(pred)
        #    print(item)
        return pred
       
    def debug(self):
        return self.pparams['lasso'], self.pparams['cols']


class BaggingRegressorModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        X = solved_attempts.drop(["id", "student"], axis=1)
        '''
        lasso_params = {
            'alpha': [0.001, 0.01, 0.05, 0.1], 
            'max_iter': [1000]}
        lasso = GridSearchCV(ElasticNet(), param_grid=lasso_params).fit(X, time)
        '''
        cols = X.columns
        lasso = BaggingRegressor().fit(X, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'lasso': lasso,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf,
            'cols': cols
        }
    
    def predict(self, attempt):
        lasso = self.pparams['lasso']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        X = attempt.drop(["id", "student"])

        pred = lasso.predict([X])[0]
        item = item_mean_times.get(attempt["item"])
        #if attempt["student"] in student_perf:
        #if abs(pred - item) > 1:
        #    print(attempt)
        #    print(pred)
        #    print(item)
        return pred
       
    def debug(self):
        return self.pparams['lasso'], self.pparams['cols']

class BayesianRidgeModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        X = solved_attempts.drop(["id", "student"], axis=1)
        '''
        lasso_params = {
            'alpha': [0.001, 0.01, 0.05, 0.1], 
            'max_iter': [1000]}
        lasso = GridSearchCV(ElasticNet(), param_grid=lasso_params).fit(X, time)
        '''
        cols = X.columns
        lasso = BayesianRidge().fit(X, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'lasso': lasso,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf,
            'cols': cols
        }
    
    def predict(self, attempt):
        lasso = self.pparams['lasso']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        X = attempt.drop(["id", "student"])

        pred = lasso.predict([X])[0]
        item = item_mean_times.get(attempt["item"])
        #if attempt["student"] in student_perf:
        #if abs(pred - item) > 1:
        #    print(attempt)
        #    print(pred)
        #    print(item)
        return pred
       
    def debug(self):
        return self.pparams['lasso'], self.pparams['cols']