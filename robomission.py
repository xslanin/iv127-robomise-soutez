from imports import *
import os
import json
import ast
from functools import partial
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import GroupShuffleSplit, GridSearchCV
from sklearn.linear_model import LinearRegression, Ridge, Lasso, ElasticNet
from sklearn.ensemble import AdaBoostRegressor, BaggingRegressor, RandomForestRegressor
from scipy import stats
from scipy.special import inv_boxcox
from sklearn.svm import SVR
import random
from scipy.stats import boxcox

from sklearn.ensemble import RandomForestRegressor

def get_performance_columns():
    return ['solved', 'time', 'start', 'response_time_sec', 'executions', 'edits', 'program']

def load_attempts(reporting_data_from='2019-04-01', filename="attempts.csv", path='./'):
    path = os.path.join(path, filename)
    converters = {'setting': json.loads, 'item_order': ast.literal_eval}
    attempts = pd.read_csv(path, index_col='id', keep_default_na=False, parse_dates=['start'], converters=converters)
    attempts['time'] = pd.to_numeric(attempts.time)
    attempts.program.fillna('', inplace=True)
    attempts = attempts.sort_values('start')
    mask_reporting = attempts.start >= pd.Timestamp(reporting_data_from, tz='CET')   
    data = {
        'attempts': attempts[~mask_reporting],
        'attempts_reporting': attempts[mask_reporting],
        'performance_columns': attempts.loc[:, 'solved':].columns,  # all from 'solved' to the right
    }
    return data

def load_attempts_time(filename='attempts_time.csv', reporting_data_from=98226, path='./'):
    path = os.path.join(path, filename)
    converters = {'setting': json.loads, 'item_order': ast.literal_eval}
    attempts = pd.read_csv(path, index_col='id', keep_default_na=False, converters=converters)
    data = {
        'attempts': attempts.loc[:(reporting_data_from-1)],
        'attempts_reporting': attempts.loc[reporting_data_from:],
        'performance_columns': get_performance_columns()
    }
    return data

class StudentModel:
    """Interface for student models.

    Concrete student models should implement:
    - fit (for offline learning; optional)
    - update (for online learning; optional)
    - predict (required)

    Attributes:
    - hparams (required, pd.Series): hyper-parameters
    - pparams (optional): parameters for the whole population
    - sparams (optional): parameters for individual students
    """
    def __init__(self, hparams=None):
        self.hparams = pd.Series(hparams)
        self.pparams = None 
        self.sparams = None
    
    def fit(self, attempts):
        """Offline learning.
        """
        pass

    def fit_online(self, attempts):
        """Online learning from multiple attempts.
        """
        for _, attempt in attempts.iterrows():
            self.update(attempt)

    def update(self, attempt):
        """Online learning from a single attempt.
        """
        pass

    def predict(self, attempt):
        """Return the predicted time of the attempt.
        """
        pass

    def predict_seconds(self, attempt):
        return np.exp(self.predict(attempt))

    def __str__(self):
        return hyphenate(self.__class__.__name__.replace('Model', ''))

    def __repr__(self):
        hparams_repr = '' if self.hparams.empty else f'{self.hparams.to_dict()}'
        return f'{self.__class__.__name__}({hparams_repr})'

# Source (modified): https://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-snake-case
def hyphenate(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1-\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1-\2', s1).lower()


def eval_models(models, attempts, scenarios=None, n_experiments_per_scenario=5):
    """Evaluates multiple models in multiple scenarios.
    
    A student-blocked cross-validation is used, with number of splits defined
    by `n_experiments_per_scenario`.
    """
    models_names = [repr(model) for model in models]
    scenarios = get_scenarios(scenarios)
    scenarios_names = [scenario['name'] for scenario in scenarios]
    results = {}
    for model in models:
        for scenario in scenarios:
            RMSE, RMSE_std = eval_single_model(model, attempts, scenario, n_experiments_per_scenario)
            results[str(model)+"-"+str(scenario['name'])] = {}
            results[str(model)+"-"+str(scenario['name'])]["mean"] = RMSE
            results[str(model)+"-"+str(scenario['name'])]["std"] = RMSE_std
    return results


def eval_single_model(model, attempts, scenario, n_experiments):
    """Evaluates a single model in a single scenario `n_experiments` times.
    
    A student-blocked cross-validation is used, with number of splits defined
    by `n_experiments`. Prints the results and returns the mean RMSE.
    """
    print(f'Evaluating {model!r}.\nScenario: {scenario["name"]!r}.\nExperiments:', end=' ')
    results = []
    cv_iterator = GroupShuffleSplit(n_splits=n_experiments, test_size=0.2)
    cv_splits = cv_iterator.split(attempts, groups=attempts.student)
    for train_IDs, test_IDs in cv_splits:
        train_attempts = attempts.iloc[train_IDs]
        test_attempts = attempts.iloc[test_IDs]
        metrics, preds = eval_model_once_in_scenario(model, train_attempts, test_attempts, scenario)
        results.append(metrics)
        print('.', end=' ')
    results = pd.DataFrame.from_records(results)
    RMSE = results['RMSE'].mean()
    RMSE_std = results['RMSE'].std()
    #RMSE_train = results['RMSE_train'].mean()
    print(f'Done!\nResults: RMSE {RMSE:0.3f} (std {RMSE_std:0.3f})\n')
    return RMSE, RMSE_std


def eval_model_once_in_scenario(model, train_attempts, test_attempts, scenario):
    """    
    The given abstract model is offline-fitted on the `train_attempts`, then
    online-updated-and-evaluated on other attempts, which are sampled from
    `test_attempts` according to the given scenario. Finally, the predictions
    are compared to the true times, the errors are weighted according to the
    scenario and a series of metrics is returned (currently only RMSE).
    You can add your own metrics, but don't change anything else in this
    function as it is also used by the evaluation for seminar reports.
    """
    model.fit(train_attempts)
    if 'test_population_sampler' in scenario:
        test_attempts = scenario['test_population_sampler'](test_attempts)
    predictions = []
    for i_attempt, attempt in test_attempts.iterrows():
        attempt_at_start = attempt.drop(get_performance_columns(), errors='ignore')
        if attempt.solved:
            predictions.append(model.predict(attempt_at_start))
        model.update(attempt)
    solved_test_attempts = test_attempts[test_attempts.solved]
    weights = None
    if 'test_population_weights' in scenario:
        weights = scenario['test_population_weights'](solved_test_attempts)
    #print(predictions)
    RMSE = compute_RMSE(solved_test_attempts.time, predictions, weights=weights)
    return pd.Series(dict(RMSE=RMSE)), predictions

def eval_models_for_seminar_report(models, train, test):
    """Evaluates several (abstract) models for the seminar reports.

    Each model is offline-fitted on the development set and then online
    updated-and-evaluated on the reporting set. This is repeated for several
    scenarios which define how to sample and weight test attempts. 
    """
    models_names = [repr(model) for model in models]
    scenarios = get_scenarios('as-observed')
    scenarios_names = [scenario['name'] for scenario in scenarios]
    results = {}
    for model in models:
        for scenario in scenarios:
            print(f'Evaluating {model!r}.\nScenario: {scenario["name"]!r}.')
            #train, test = DATA['attempts'], DATA['attempts_reporting']
            metrics, predictions = eval_model_once_in_scenario(model, train, test, scenario)
            RMSE = metrics['RMSE']
            print(f'Results: RMSE {RMSE:0.3f}\n')
            results[str(model)+"-"+str(scenario['name'])] = {}
            results[str(model)+"-"+str(scenario['name'])]["mean"] = metrics['RMSE']
    return results, predictions


def filter_students(attempts, min_attempts):
    """Filter students with less than `min_attempts` attempts.
    """
    attempts = attempts.groupby('student').filter(lambda g: len(g) >= min_attempts)
    if attempts.empty:
        raise ValueError('No attempts left after filtering of students. '
                         'Use more data or disable this scenario.')
    return attempts

def get_scenarios(names=None):
    SCENARIOS = (
        {'name': 'as-observed'},
        {'name': 'students>20', 'test_population_sampler': partial(filter_students, min_attempts=20)}
    )
    return [scenario for scenario in SCENARIOS if names is None or scenario['name'] in names]

def compute_RMSE(y_true, y_pred, weights=None):
    return np.sqrt(mean_squared_error(y_true, y_pred, sample_weight=weights))

def print_score(m, X_train, y_train, X_valid, y_valid):
    res = [compute_RMSE(m.predict(X_train), y_train), compute_RMSE(m.predict(X_valid), y_valid), m.score(X_train, y_train), m.score(X_valid, y_valid)]
    return res

class GlobalAvgModel(StudentModel):
    """Always predicts the global success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        self.pparams = {'global_mean_time': solved_attempts.time.mean()}
    
    def predict(self, attempt):
        return self.pparams['global_mean_time']

class ItemAvgModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]     
        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': solved_attempts.groupby('item').time.mean()
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        global_mean_time = self.pparams['global_mean_time']
        # attempt.item() is a method of pd.Series, so 'item' notation is required.
        return item_mean_times.get(attempt['item'], default=global_mean_time)

class SVRRegressionModelOnlySeen(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        '''
        lasso_params = {
            'alpha': [0.001, 0.01, 0.05, 0.1], 
            'max_iter': [1000]}
        lasso = GridSearchCV(SVR(), param_grid=lasso_params).fit(solved_attempts, time)
        '''
        svr = SVR().fit(solved_attempts, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True

        self.pparams = {
            'svr': svr,
            'item_mean_times': item_mean_times,
            'student_perf': student_perf
        }
    
    def predict(self, attempt):
        svr = self.pparams['svr']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        #print(attempt)
        pred = svr.predict([attempt])[0]
        item = item_mean_times.get(attempt["item"])
        if attempt["student"] in student_perf:
            if pred > item:
            #    return pred
            #else:
            #    return item
                return pred
        return item

    def debug(self):
        return self.pparams['svr'].best_estimator_