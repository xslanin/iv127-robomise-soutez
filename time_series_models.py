from robomission import *

class GlobalAvgModelHour(StudentModel):
    """Always predicts the global success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),
            'global_hour_mean_times': solved_attempts.groupby('startHour').time.mean()
        }
    
    def predict(self, attempt):
        global_hour_mean_times = self.pparams['global_hour_mean_times']
        return global_hour_mean_times.get(attempt['startHour'], default=self.pparams['global_hour_mean_times'])

class ItemAvgModelHourWeighted(StudentModel):
    """Predicts the item success rate estimated offline.
    """      
    def fit(self, attempts, weight = 0.25):
        solved_attempts = attempts[attempts.solved]

        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': solved_attempts.groupby('item').time.mean(),
            'item_time_mean_times': solved_attempts.groupby(['item', 'startHour']).time.mean(),
            'weight': weight
        }

    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        item_time_mean_times = self.pparams['item_time_mean_times']
        mean_per_task = item_time_mean_times[attempt['item']].values.mean()
        global_mean_time = self.pparams['global_mean_time']
        #for task in self.pparams['item_mean_times'].index.unique():
        task = attempt["item"]
        if not task in item_time_mean_times:
            return global_mean_time
        if attempt['startHour'] in item_time_mean_times[task]:
            diff = item_time_mean_times[task][attempt['startHour']] - mean_per_task
            addition_term = abs(diff)*self.pparams['weight']
            if diff > 0:
                return mean_per_task + addition_term
            else:
                return mean_per_task - addition_term
        else:
            return mean_per_task

    def debug(self):
        return self.pparams['item_time_mean_times']


class ItemAvgModelHour(StudentModel):
    """Predicts the item success rate estimated offline.
    """      
    def fit(self, attempts, weight = 1):
        solved_attempts = attempts[attempts.solved]

        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': solved_attempts.groupby('item').time.mean(),
            'item_time_mean_times': solved_attempts.groupby(['item', 'startHour']).time.mean(),
            'weight': weight
        }

    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        item_time_mean_times = self.pparams['item_time_mean_times']
        mean_per_task = item_time_mean_times[attempt['item']].values.mean()
        global_mean_time = self.pparams['global_mean_time']
        #for task in self.pparams['item_mean_times'].index.unique():
        task = attempt["item"]
        if not task in item_time_mean_times:
            return global_mean_time
        if attempt['startHour'] in item_time_mean_times[task]:
            return item_time_mean_times[task][attempt['startHour']]
        else:
            return mean_per_task

    def debug(self):
        return self.pparams['item_time_mean_times']

class ItemAvgModelTimeOfTheDayWeighted(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts, weight=0.25):
        solved_attempts = attempts[attempts.solved]

        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': solved_attempts.groupby('item').time.mean(),
            'item_time_mean_time_morning':  solved_attempts[solved_attempts["isMorning"] == True].groupby(['item', 'isMorning']).time.mean(),
            'item_time_mean_time_afternoon':  solved_attempts[solved_attempts["isAfternoon"] == True].groupby(['item', 'isAfternoon']).time.mean(),
            'item_time_mean_time_evening':  solved_attempts[solved_attempts["isEvening"] == True].groupby(['item', 'isEvening']).time.mean(),
            'item_time_mean_time_night':  solved_attempts[solved_attempts["isNight"] == True].groupby(['item', 'isNight']).time.mean(),
            'weight': weight
        }

    def predict(self, attempt):
        if attempt['isMorning'] and attempt["item"] in self.pparams['item_time_mean_time_morning']:
            diff = self.pparams['item_mean_times'][attempt["item"]] - self.pparams['item_time_mean_time_morning'][attempt["item"]].values[0]
            addition_term = abs(diff)*self.pparams['weight'] 
            if diff > 0:
                return self.pparams['item_mean_times'][attempt["item"]] + addition_term
            else:
                return self.pparams['item_mean_times'][attempt["item"]] - addition_term
        if attempt['isAfternoon'] and attempt["item"] in self.pparams['item_time_mean_time_afternoon']:
            diff = self.pparams['item_mean_times'][attempt["item"]] - self.pparams['item_time_mean_time_afternoon'][attempt["item"]].values[0]
            addition_term = abs(diff)*self.pparams['weight']
            if diff > 0:
                return self.pparams['item_mean_times'][attempt["item"]] + addition_term
            else:
                return self.pparams['item_mean_times'][attempt["item"]] - addition_term
        if attempt['isEvening'] and attempt["item"] in self.pparams['item_time_mean_time_evening']: 
            diff = self.pparams['item_mean_times'][attempt["item"]] - self.pparams['item_time_mean_time_evening'][attempt["item"]].values[0]
            addition_term = abs(diff)*self.pparams['weight']
            if diff > 0:
                return self.pparams['item_mean_times'][attempt["item"]] + addition_term
            else:
                return self.pparams['item_mean_times'][attempt["item"]] - addition_term
        if attempt['isNight'] and attempt["item"] in self.pparams['item_time_mean_time_night']:
            diff = self.pparams['item_mean_times'][attempt["item"]] - self.pparams['item_time_mean_time_night'][attempt["item"]].values[0]
            addition_term = abs(diff)*self.pparams['weight']
            if diff > 0:
                return self.pparams['item_mean_times'][attempt["item"]] + addition_term
            else:
                return self.pparams['item_mean_times'][attempt["item"]] - addition_term
        else:
            return  self.pparams['global_mean_time']

    def debug(self):
        return self.pparams['item_time_mean_time_morning']

class ItemAvgModelTimeOfTheDay(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]

        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': solved_attempts.groupby('item').time.mean(),
            'item_time_mean_time_morning':  solved_attempts[solved_attempts["isMorning"] == True].groupby(['item', 'isMorning']).time.mean(),
            'item_time_mean_time_afternoon':  solved_attempts[solved_attempts["isAfternoon"] == True].groupby(['item', 'isAfternoon']).time.mean(),
            'item_time_mean_time_evening':  solved_attempts[solved_attempts["isEvening"] == True].groupby(['item', 'isEvening']).time.mean(),
            'item_time_mean_time_night':  solved_attempts[solved_attempts["isNight"] == True].groupby(['item', 'isNight']).time.mean(),
        }

    def predict(self, attempt):
        if attempt['isMorning'] and attempt["item"] in self.pparams['item_time_mean_time_morning']:
            return self.pparams['item_time_mean_time_morning'][attempt["item"]].values[0]
        if attempt['isAfternoon'] and attempt["item"] in self.pparams['item_time_mean_time_afternoon']:
            return self.pparams['item_time_mean_time_afternoon'][attempt["item"]].values[0]
        if attempt['isEvening'] and attempt["item"] in self.pparams['item_time_mean_time_evening']: 
            return self.pparams['item_time_mean_time_evening'][attempt["item"]].values[0]
        if attempt['isNight'] and attempt["item"] in self.pparams['item_time_mean_time_night']:
            return self.pparams['item_time_mean_time_night'][attempt["item"]].values[0]
        else:
            return  self.pparams['global_mean_time']

    def debug(self):
        return self.pparams['item_time_mean_time_morning']

class ItemAvgModelMonths(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]     
        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': solved_attempts.groupby('item').time.mean(),
            'item_mean_times_per_month': solved_attempts.groupby(['item', 'startMonth']).time.mean()
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        item_mean_times_per_month = self.pparams['item_mean_times_per_month']
        global_mean_time = self.pparams['global_mean_time']
        if attempt["item"] in item_mean_times_per_month:
            if attempt["startMonth"] in item_mean_times_per_month[attempt["item"]]:
                return item_mean_times_per_month[attempt["item"]][attempt["startMonth"]]
        # attempt.item() is a method of pd.Series, so 'item' notation is required.
        return item_mean_times.get(attempt['item'], default=global_mean_time)

class ItemAvgModelSeasons(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]     
        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': solved_attempts.groupby('item').time.mean(),
            'item_mean_times_per_month': solved_attempts.groupby(['item', 'startMonth']).time.mean()
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        item_mean_times_per_month = self.pparams['item_mean_times_per_month']
        global_mean_time = self.pparams['global_mean_time']
        if attempt["item"] in item_mean_times_per_month:
            if attempt["startMonth"] in item_mean_times_per_month[attempt["item"]]:
                if attempt["startMonth"] >= 7 and attempt["startMonth"] <=8:
                    return item_mean_times_per_month[attempt["item"]][6:8].mean()
                else:
                    return item_mean_times_per_month[attempt["item"]][3:6].mean()
        # attempt.item() is a method of pd.Series, so 'item' notation is required.
        return item_mean_times.get(attempt['item'], default=global_mean_time)

class ItemAvgModelWeekend(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]     
        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': solved_attempts.groupby('item').time.mean(),
            'item_mean_times_weekend': solved_attempts.groupby(['item', 'isWeekend']).time.mean()
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        item_mean_times_weekend = self.pparams['item_mean_times_weekend']
        global_mean_time = self.pparams['global_mean_time']
        if attempt["item"] in item_mean_times_weekend:
            return item_mean_times_weekend[attempt["item"]][attempt["isWeekend"]].mean()
        # attempt.item() is a method of pd.Series, so 'item' notation is required.
        return item_mean_times.get(attempt['item'], default=global_mean_time)

class ItemAvgModelSchool(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]     
        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': solved_attempts.groupby('item').time.mean(),
            'item_mean_times_school': solved_attempts.groupby(['item', 'isSchoolWorkingHours']).time.mean(),
            'item_mean_times_school_break': solved_attempts.groupby(['item', 'isSchoolBreak']).time.mean()
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        item_mean_times_school = self.pparams['item_mean_times_school']
        item_mean_times_school_break = self.pparams['item_mean_times_school_break']
        global_mean_time = self.pparams['global_mean_time']
        if attempt["item"] in item_mean_times_school:
            if attempt["isSchoolWorkingHours"] and attempt["isSchoolWorkingHours"] in item_mean_times_school[attempt["item"]]:
                return item_mean_times_school[attempt["item"]][attempt["isSchoolWorkingHours"]].mean()
            if attempt["isSchoolBreak"] in item_mean_times_school_break[attempt["item"]]:
                return item_mean_times_school_break[attempt["item"]][attempt["isSchoolBreak"]].mean()
        # attempt.item() is a method of pd.Series, so 'item' notation is required.
        return item_mean_times.get(attempt['item'], default=global_mean_time)
