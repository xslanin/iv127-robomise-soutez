from robomission import *
from sklearn.neighbors import KNeighborsRegressor
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler
from sklearn.decomposition import PCA
# TODO Clustering na sposob clustre typickych uzivatelov podla casu, i guess

# TODO Clustering na sposob clustre pomalsich vs. rychlejsich


class KNeighborsRegressorModel(StudentModel):
    # TODO: https://codingdisciple.com/predict-car-price-kneighbors.html
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        cols = solved_attempts.columns
        #params = {"n_neighbors": [2,5,10,15]}
        model = KNeighborsRegressor(n_neighbors=5)
        model.fit(solved_attempts, time)
        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True
        self.pparams = {
            'model': model,
            'item_mean_times': item_mean_times,
            'cols': cols,
            'student_perf': student_perf
        }
    
    def predict(self, attempt):
        model = self.pparams['model']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        pred = model.predict([attempt])[0]
        item = item_mean_times.get(attempt["item"])
        if attempt["item"] in student_perf:
            if pred > item:
                return pred
        return item
    def debug(self):
        return self.pparams['model'], self.pparams['cols']


class PCRModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        item_mean_times = solved_attempts.groupby('item').time.mean()
        time = solved_attempts[["time"]]
        solved_attempts = solved_attempts.drop(get_performance_columns(), axis=1)
        cols = solved_attempts.columns
        #params = {"n_neighbors": [2,5,10,15]}
        
        scaler = StandardScaler()
        X_train_scaled = scaler.fit_transform(solved_attempts)
        model = PCA(n_components=len(solved_attempts.columns), whiten=True)
        model.fit(X_train_scaled)
        x_pca = model.transform(X_train_scaled)
        linreg = LinearRegression().fit(x_pca, time)

        grouped = solved_attempts.groupby("student")
        student_perf = {}
        for key,group in grouped:
            student_perf[key] = True
        self.pparams = {
            'model': linreg,
            'item_mean_times': item_mean_times,
            'cols': cols,
            'student_perf': student_perf
        }
    
    def predict(self, attempt):
        model = self.pparams['model']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        linreg = self.pparams['model']

        scaler = StandardScaler()
        X_train_scaled = scaler.fit_transform([attempt])
        model = PCA(n_components=len(self.pparams['cols']), whiten=True)
        model.fit(X_train_scaled)
        x_pca = model.transform(X_train_scaled)

        pred = linreg.predict([attempt])[0]
        item = item_mean_times.get(attempt["item"])
        #if attempt["item"] in student_perf:
            #if pred > item:
        return pred
        #return item
    def debug(self):
        return self.pparams['model'], self.pparams['cols']
