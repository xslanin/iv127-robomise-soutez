from robomission import *

class ItemAvgModelStart(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]     
        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': solved_attempts.groupby('item').time.mean(),
            'seen_students_in_test': []
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        global_mean_time = self.pparams['global_mean_time']
        # attempt.item() is a method of pd.Series, so 'item' notation is required.
        worst_tasks_to_start_with = [10, 12, 14, 15, 22, 26, 29, 31, 33, 36, 38, 42, 44, 48, 64, 67, 78, 83, 86]
        handicap = 1
        if not attempt['student'] in self.pparams['seen_students_in_test']:
            if attempt['item'] in worst_tasks_to_start_with:
                handicap = 1.25
            self.pparams['seen_students_in_test'].append(attempt['student'])
        r = item_mean_times.get(attempt['item'], default=global_mean_time)*handicap
        return r