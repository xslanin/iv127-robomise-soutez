from robomission import *

class ItemAvgModelBoxCox(StudentModel):
    """Predicts the item success based on student's expected performance
    and by recomputing the distribution using boxcox

    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        solved_attempts = solved_attempts[solved_attempts["time"] > 0.0]
        grouped = solved_attempts.groupby('student')
        student_perf = {}
        solved_attempts['time_boxcox'] = boxcox(solved_attempts["time"], lmbda=None)[0]
        mean_times = solved_attempts.groupby('item').time_boxcox.mean()
        for key,group in grouped:
            time = group["time"].values
            items = mean_times.get(group["item"]).values
            item_diffs = abs(np.array(mean_times).mean() - items)
            student_perf[key] = (item_diffs*np.array(time - items)).mean()

        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': mean_times,
            'seen_students_in_test': [],
            'student_perf': student_perf
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        global_mean_time = self.pparams['global_mean_time']
        student_perf = self.pparams['student_perf']
        worse_tasks_to_start_with = [2, 3, 10, 12, 13, 14, 15, 22, 26, 29, 31, 33, 36, 38, 42, 44, 48, 49, 64, 67, 78, 83, 86]
        handicap = 1
        if not attempt['student'] in self.pparams['seen_students_in_test']:
            if attempt['item'] in worse_tasks_to_start_with:
                handicap = 1.0
            self.pparams['seen_students_in_test'].append(attempt['student'])

        if attempt['student'] in student_perf:
            r = inv_boxcox(item_mean_times.get(attempt['item'], default=global_mean_time), 0)*handicap
            return r
        else:
            r = inv_boxcox(item_mean_times.get(attempt['item'], default=global_mean_time), 0)*handicap
            return r

        # attempt.item() is a method of pd.Series, so 'item' notation is required.
        #return np.exp(item_mean_times.get(attempt['item'], default=global_mean_time))
        return inv_boxcox(item_mean_times.get(attempt['item'], default=global_mean_time), 0)

    def debug(self):
        return self.pparams['student_perf']

class ItemAvgModelBoxCoxWorseTasksToStartWith(StudentModel):
    """Predicts the item success based on student's expected performance
    and by recomputing the distribution using boxcox

    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        solved_attempts = solved_attempts[solved_attempts["time"] > 0.0]
        grouped = solved_attempts.groupby('student')
        student_perf = {}
        solved_attempts['time_boxcox'] = boxcox(solved_attempts["time"], lmbda=None)[0]
        mean_times = solved_attempts.groupby('item').time_boxcox.mean()
        for key,group in grouped:
            time = group["time"].values
            items = mean_times.get(group["item"]).values
            item_diffs = abs(np.array(mean_times).mean() - items)
            student_perf[key] = (item_diffs*np.array(time - items)).mean()

        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': mean_times,
            'seen_students_in_test': [],
            'student_perf': student_perf
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        global_mean_time = self.pparams['global_mean_time']
        student_perf = self.pparams['student_perf']
        worse_tasks_to_start_with = [2, 3, 10, 12, 13, 14, 15, 22, 26, 29, 31, 33, 36, 38, 42, 44, 48, 49, 64, 67, 78, 83, 86]
        handicap = 1
        if not attempt['student'] in self.pparams['seen_students_in_test']:
            if attempt['item'] in worse_tasks_to_start_with:
                handicap = 1.2
            self.pparams['seen_students_in_test'].append(attempt['student'])

        if attempt['student'] in student_perf:
            r = inv_boxcox(item_mean_times.get(attempt['item'], default=global_mean_time), 0)*handicap
            return r
        else:
            r = inv_boxcox(item_mean_times.get(attempt['item'], default=global_mean_time), 0)*handicap
            return r

        # attempt.item() is a method of pd.Series, so 'item' notation is required.
        #return np.exp(item_mean_times.get(attempt['item'], default=global_mean_time))
        return inv_boxcox(item_mean_times.get(attempt['item'], default=global_mean_time), 0)

    def debug(self):
        return self.pparams['student_perf']


class ItemAvgModelWorseTasksToStartWith(StudentModel):
    """Predicts the item success based on student's expected performance
    and by recomputing the distribution using boxcox

    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        solved_attempts = solved_attempts[solved_attempts["time"] > 0.0]
        grouped = solved_attempts.groupby('student')
        student_perf = {}
        #solved_attempts['time_boxcox'] = boxcox(solved_attempts["time"], lmbda=None)[0]
        mean_times = solved_attempts.groupby('item').time.mean()
        for key,group in grouped:
            time = group["time"].values
            items = mean_times.get(group["item"]).values
            item_diffs = abs(np.array(mean_times).mean() - items)
            student_perf[key] = (item_diffs*np.array(time - items)).mean()

        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': mean_times,
            'seen_students_in_test': [],
            'student_perf': student_perf
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        global_mean_time = self.pparams['global_mean_time']
        student_perf = self.pparams['student_perf']
        worse_tasks_to_start_with = [2, 3, 10, 12, 13, 14, 15, 22, 26, 29, 31, 33, 36, 38, 42, 44, 48, 49, 64, 67, 78, 83, 86]
        handicap = 1
        if not attempt['student'] in self.pparams['seen_students_in_test']:
            if attempt['item'] in worse_tasks_to_start_with:
                handicap = 1.2
            self.pparams['seen_students_in_test'].append(attempt['student'])

        if attempt['student'] in student_perf:
            r = item_mean_times.get(attempt['item'], default=global_mean_time)*handicap
            return r
        else:
            r = item_mean_times.get(attempt['item'], default=global_mean_time)*handicap
            return r

        # attempt.item() is a method of pd.Series, so 'item' notation is required.
        #return np.exp(item_mean_times.get(attempt['item'], default=global_mean_time))
        return item_mean_times.get(attempt['item'], default=global_mean_time)

    def debug(self):
        return self.pparams['student_perf']


class ItemAvgModelBoxCoxWorseTasksToStartWithLinReg(StudentModel):
    """Predicts the item success based on student's expected performance
    and by recomputing the distribution using boxcox

    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        solved_attempts = solved_attempts[solved_attempts["time"] > 0.0]
        grouped = solved_attempts.groupby('student')
        student_perf = {}
        solved_attempts['time_boxcox'] = boxcox(solved_attempts["time"], lmbda=None)[0]
        
        time = solved_attempts[['time']]
        solved_attempts_ = solved_attempts.drop(get_performance_columns(), axis=1)
        linreg = LinearRegression().fit(solved_attempts_.drop(['time_boxcox'], axis=1), time)
        
        mean_times = solved_attempts.groupby('item').time_boxcox.mean()
        for key,group in grouped:
            time = group["time"].values
            items = mean_times.get(group["item"]).values
            item_diffs = abs(np.array(mean_times).mean() - items)
            student_perf[key] = (item_diffs*np.array(time - items)).mean()

        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': mean_times,
            'seen_students_in_test': [],
            'student_perf': student_perf,
            'linreg': linreg
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        global_mean_time = self.pparams['global_mean_time']
        student_perf = self.pparams['student_perf']
        linreg = self.pparams['linreg']
        worse_tasks_to_start_with = [2, 3, 10, 12, 13, 14, 15, 22, 26, 29, 31, 33, 36, 38, 42, 44, 48, 49, 64, 67, 78, 83, 86]
        handicap = 1
        if not attempt['student'] in self.pparams['seen_students_in_test']:
            if attempt['item'] in worse_tasks_to_start_with:
                handicap = 1.2
            self.pparams['seen_students_in_test'].append(attempt['student'])

        if attempt['student'] in student_perf:
            pred = linreg.predict([attempt])[0]
            pred_ = inv_boxcox(pred,0)
            #r = inv_boxcox(pred,0)*handicap
            item = inv_boxcox(item_mean_times.get(attempt['item'], default=global_mean_time), 0)
            if pred > item:
                return pred*handicap
            else:
                return item*handicap
        else:
            r = inv_boxcox(item_mean_times.get(attempt['item'], default=global_mean_time), 0)*handicap
            return r

    def debug(self):
        return self.pparams['student_perf']

class ItemAvgModelBoxCoxPerItem(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        solved_attempts = solved_attempts[solved_attempts["time"] > 0.0]
        grouped = solved_attempts.groupby('item')
        solved_attempts['time_boxcox'] = boxcox(solved_attempts["time"], lmbda=None)[0]
        #mean_times = solved_attempts.groupby('item').time_boxcox.mean()
        mean_times = {}
        for key, item in grouped:
            current_df = grouped.get_group(key)[["time_boxcox"]]
            z = np.abs(stats.zscore(current_df))
            threshold = z.max() - z.max()*0.025
            threshold2 = z.min() - z.min()*0.025
            z_score_df = current_df[((z < threshold) & (z > threshold2)).all(axis=1)]
            mean_times[key] = z_score_df['time_boxcox'].mean()
        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': mean_times
        }
        
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        global_mean_time = self.pparams['global_mean_time']
        return inv_boxcox(item_mean_times.get(attempt['item']), 0)
        #global_mean_time = self.pparams['global_mean_time']
        # attempt.item() is a method of pd.Series, so 'item' notation is required.
        #return np.exp(item_mean_times.get(attempt['item'], default=global_mean_time))
        #return inv_boxcox(item_mean_times.get(attempt['item'], default=global_mean_time), 0)

    def debug(self):
        return self.pparams['item_mean_times']

class ItemAvgModelOutliersRemoval(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        solved_attempts = solved_attempts.drop(["start"], axis=1)
        grouped = solved_attempts.groupby('item')
        mean_times = {}
        for key, item in grouped:
            current_df = grouped.get_group(key)[["time"]]
            z = np.abs(stats.zscore(current_df))
            threshold = z.max() - z.max()*0.025
            threshold2 = z.min() - z.min()*0.025
            z_score_df = current_df[((z < threshold) & (z > threshold2)).all(axis=1)]
            mean_times[key] = z_score_df['time'].mean()
        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': mean_times
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        return item_mean_times[attempt['item']]

    def debug(self):
        return self.pparams['item_mean_times']

class ItemAvgModelOutliersRemovalCombination(StudentModel):
    """Info:
    Combination of:
    1. Outlier detection and removal (slightly better RMSE than plain Item Avg)
    2. Mean of (times of student seen in training set per task - times of task averages)
        (little effect, since students seen in train as well as in test form only 3%  of test dataset)
    3. Punishing 'hard first tasks': Mean of tasks that were solved as the first tasks for a student ever (hardcoded for now)
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]
        solved_attempts = solved_attempts.drop(["start"], axis=1)
        item_times = solved_attempts.groupby('item').time.mean()
        grouped = solved_attempts.groupby('item')
        mean_times = {}
        for key, item in grouped:
            current_df = grouped.get_group(key)[["time"]]
            z = np.abs(stats.zscore(current_df))
            threshold = z.max() - z.max()*0.05
            threshold2 = z.min() - z.min()*0.05
            z_score_df = current_df[((z < threshold) & (z > threshold2)).all(axis=1)]
            mean_times[key] = z_score_df['time'].mean()

        mean_times = pd.Series(mean_times)
        grouped = solved_attempts.groupby('student')
        student_perf = {}
        for key,group in grouped:
            time = group["time"].values
            items = mean_times.get(group["item"]).values
            item_diffs = abs(np.array(mean_times).mean() - items)
            student_perf[key] = (item_diffs*np.array(time - items)).mean()
        
        perc_solved = {}
        for item in attempts["item"].unique():
            perc_solved[item] = len(attempts[(attempts["item"] == item) & (attempts["solved"] == False)].values) / len(attempts[(attempts["item"] == item) & (attempts["solved"] == True)].values)
        perc_solved = pd.Series(perc_solved)

        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': mean_times,
            'student_perf': student_perf,
            'seen_students_in_test': []
        }
    
    def predict(self, attempt):
        global_mean_time = self.pparams['global_mean_time']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        worse_tasks_to_start_with = [2, 3, 10, 12, 13, 14, 15, 22, 26, 29, 31, 33, 36, 38, 42, 44, 48, 49, 64, 67, 78, 83, 86]
        better_tasks_to_start_with = [19, 24, 46, 57, 58, 62, 63, 65, 68, 71, 75, 77, 81, 84, 85, ]
        handicap = 1
        if not attempt['student'] in self.pparams['seen_students_in_test']:
            if attempt['item'] in worse_tasks_to_start_with:
                handicap = 1.25
            elif attempt['item'] in better_tasks_to_start_with:
                handicap = 1.0
            self.pparams['seen_students_in_test'].append(attempt['student'])
        if attempt['student'] in student_perf:
            r = item_mean_times.get(attempt['item'], default=global_mean_time)*handicap
            return r \
             + student_perf[attempt['student']]*0.125
        else:
            r = item_mean_times.get(attempt['item'], default=global_mean_time)*handicap
            return r

    def debug(self):
        return self.pparams['seen_students_in_test']