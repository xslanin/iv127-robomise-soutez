from imports import *
from pandas.api.types import is_string_dtype, is_numeric_dtype, is_datetime64_any_dtype
from sklearn.ensemble import RandomForestRegressor
import time
from scipy.stats import logistic
import matplotlib

#with pd.option_context('display.max_rows', None, 'display.max_columns', None):

def heat_map(corrs_mat):
    sns.set(style="white")
    f, ax = plt.subplots(figsize=(8, 8))
    mask = np.zeros_like(corrs_mat, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True 
    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    sns.heatmap(corrs_mat, mask=mask, cmap=cmap, ax=ax)

def heat_map_2(df):
    f, ax = plt.subplots(figsize=(11, 9))
    corr = df.corr()
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    sns.heatmap(corr,  cmap=cmap, vmax=.3, center=0,
                square=True, linewidths=.5, cbar_kws={"shrink": .5})

def add_solving_time_mean_diff(df):
    df["solving_time_mean_diff"] = 0.0
    for name, group in df.groupby("student"):
        df.at[group.index, "solving_time_mean_diff"] = group["time"].mean() - item_averages.get(group["item"].values).mean()

def add_task_skills(df):
    df["knowsColor"] = 0.5; df["knowsLoop"] = 0.5; df["knowsPosition"] = 0.5
    for name, group in df.groupby("student"):
        knowsLoop = 0; knowsColor = 0; knowsPosition = 0
        for index, row in group.iterrows():
            df.at[index, "knowsColor"] = logistic.cdf(knowsColor*0.5)
            df.at[index, "knowsLoop"] = logistic.cdf(knowsLoop*0.5)
            df.at[index, "knowsPosition"] = logistic.cdf(knowsPosition*0.5)
            if row["isColor"]: knowsColor += 1
            if row["isLoop"]: knowsLoop += 1
            if row["isPosition"]: knowsPosition += 1
    return df

def add_task_difficulty(df):
    easy = [2,8,10,12,14,26,31,44,49,51,53, 67, 86]
    difficult = [28,33,35,40,42,45,61,62,64,74,77]
    df["itemDifficulty"] = 1
    for i, r in df.iterrows():
        if r["item"] in easy:
            df.at[i,"itemDifficulty"] = 0
        if r["item"] in difficult:
            df.at[i, "itemDifficulty"] = 2
    return df


def add_task_skills_forgetting(df):
    df["knowsColor"] = 0.5; df["knowsLoop"] = 0.5; df["knowsPosition"] = 0.5
    for name, group in df.groupby("student"):
        knowsLoop = 0; knowsColor = 0; knowsPosition = 0; forgetting = 0
        first = True
        for index, row in group.iterrows():
            time_since_solving = row["timeSinceLastSolving"] if not first else 0
            forgetting = forgetting + -0.005*(time_since_solving / 86400)
            skillColor = logistic.cdf(knowsColor*0.5)
            skillLoop = logistic.cdf(knowsLoop*0.5)
            skillPosition = logistic.cdf(knowsPosition*0.5)
            df.at[index, "knowsColor"] = skillColor + forgetting
            df.at[index, "knowsLoop"] = skillLoop + forgetting
            df.at[index, "knowsPosition"] = skillPosition + forgetting
            if row["isColor"] and row["solved"] == True: knowsColor += 1
            if row["isLoop"] and row["solved"] == True: knowsLoop += 1
            if row["isPosition"] and row["solved"] == True: knowsPosition += 1
            first = False
    return df

def get_task_change(df):
    return df[df["time"] > 8]["item"].value_counts() / df[df["time"] > 8]["item"].value_counts().sum()

def add_time_since_last_solving(df, df_train):
    df["timeSinceLastSolving"] = 0.0
    #df["longestTimeChance"] = 0.0
    task_chance = get_task_change(df_train)

    for name, group in df.groupby("student"):
        c = 0
        for index, row in group.iterrows():
            if c != 0:
                df.at[index, "timeSinceLastSolving"] = (row["start"] - prev_task_time).total_seconds()
            #df.at[index, "longestTimeChance"] = task_chance.get(row["item"])
            prev_task_time = row["start"]
            c += 1
    #df["timeSinceLastSolving"] = np.where(df["timeSinceLastSolving"] == 0.0, df["timeSinceLastSolving"].max(), df["timeSinceLastSolving"])
    #df["timeSinceLastSolving"] = np.log(df["timeSinceLastSolving"])
    return df

def add_item_average(df, df_train):
    item_averages = df_train.groupby("item").time.mean()
    vals=item_averages.get(df_train["item"]).values
    c = 0
    for index, s in df.iterrows():
        df.at[index, "itemMean"] = vals[c]
        c+=1
    return df

def add_number_of_not_solved(df):
    df["numberNotSolved"] = 0
    for name, group in df.groupby("student"):
        numberNotSolved = 0
        for index, row in group.iterrows():
            if row["solved"] == False:
                numberNotSolved += 1
            df.at[index, "numberNotSolved"] = numberNotSolved
    return df

def add_features(df, df_train):
    df = add_time_since_last_solving(df, df_train)
    df = add_task_skills(df)
    df = add_task_difficulty(df)
    #df = add_number_of_not_solved(df)
    #df = add_item_average(df, df_train)
    return df

def drop_unimportant_columns(df):
    l = ["item_solution", "event_order", "item_setting", "startDayofweek", 
        "startDayofyear", "startSecond", 'startYear',\
       'startMonth', 'startWeek', 'startDay', 'startIs_month_end',\
       'startIs_month_start', 'startIs_quarter_end', 'startIs_quarter_start',\
       'startIs_year_end', 'startIs_year_start', 'startHour', 'startMinute',\
       'startElapsed', 'isSummerHolidays', 'isStateHolidays', 'isNight', 'isSchoolBreak', \
       'isShortlyBeforeBreak', 'isWeekend', 'isAfternoon', 'isEvening', 'isSchoolWorkingHours', 'isMorning']
    dropped = df.drop(l, axis=1)
    return dropped


def get_redundant_columns():
    return ['startMonth',
 'startIs_quarter_end',
 'event_order',
 'isMorning',
 'startIs_year_start',
 'startSecond',
 'isWeekend',
 'startIs_month_end',
 'startYear',
 'isSummerHolidays',
 'startDayofweek',
 'startElapsed',
 'isShortlyBeforeBreak',
 'item_setting',
 'item_solution',
 'startIs_month_start',
 'startDayofyear',
 'startWeek',
 'isAfternoon',
 'startIs_quarter_start',
 'isEvening',
 'isStateHolidays',
 'isNight',
 'isSchoolWorkingHours',
 'isSchoolBreak',
 'startIs_year_end',
 'startMinute']

def numericalize_all(df, skip = [], max_n_cat=None):
    for n,c in df.items(): 
        if not n in skip:
            numericalize(df, c, n, max_n_cat)

# Taken from fast.ai - https://github.com/fastai/fastai/blob/72286b8b22284a53b07d777783ff5d392a3d45b0/old/fastai/structured.py
def numericalize(df, col, name, max_n_cat):
    if max_n_cat:
        to_categorical(df)
    if not is_numeric_dtype(col) and not is_datetime64_any_dtype(col) and ( max_n_cat is None or len(col.cat.categories)>max_n_cat):
        df[name] = pd.Categorical(col).codes+1

def add_datepart(df, fldname, drop=False, time=True, errors="raise"):	
    fld = df[fldname]
    fld_dtype = fld.dtype
    if isinstance(fld_dtype, pd.core.dtypes.dtypes.DatetimeTZDtype):
        fld_dtype = np.datetime64

    if not np.issubdtype(fld_dtype, np.datetime64):
        df[fldname] = fld = pd.to_datetime(fld, infer_datetime_format=True, errors=errors)
    targ_pre = re.sub('[Dd]ate$', '', fldname)
    attr = ['Year', 'Month', 'Week', 'Day', 'Dayofweek', 'Dayofyear',
            'Is_month_end', 'Is_month_start', 'Is_quarter_end', 'Is_quarter_start', 'Is_year_end', 'Is_year_start']
    if time: attr = attr + ['Hour', 'Minute', 'Second']
    for n in attr: df[targ_pre + n] = getattr(fld.dt, n.lower())
    df[targ_pre + 'Elapsed'] = fld.astype(np.int64) // 10 ** 9
    if drop: df.drop(fldname, axis=1, inplace=True)

def df_preprocess(df, skip=[]):
    df_copy = df_preprocess_basic(df, skip)
    df_copy = add_concepts(df_copy)
    return df_copy

def df_preprocess_basic(df, skip=[]):
    df_copy = df.copy()
    add_datepart(df_copy, 'start')
    numericalize_all(df_copy, skip)
    preprocess_time(df_copy)
    return df_copy

def add_concepts(df):
    df["isLoop"] = False
    df["isColor"] = False
    df["isPosition"] = False
    for index, v in df.iterrows():
        sol = v["item_solution"]
        if "y" in sol or "!y" in sol:
            df.at[index, "isColor"] = True
        if "x=" in sol or "x>" in sol or "x<" in sol:
             df.at[index, "isPosition"] = True
        if "W" in sol or "R" in sol or "I" in sol or "/" in sol:
             df.at[index, "isLoop"] = True
    return df

def preprocess_time(df):
    df["isSummerHolidays"] = df["startMonth"].between(7,8, inclusive=True)
    state_holidays = [(1,1), (1,5), (8,5), (5,7), (6,7), (28,9), (28,10), (17,11), (24,12), (25, 12), (26,12)]
    state_holidays_year_dependent = [(19,4,2019), (22,4,2019), (30,3,2018), (2,4,2018)]
    df["isStateHolidays"] = False
    for year in list(df["startYear"].unique()):
        for holidays in state_holidays:
            df.loc[(df["startYear"]== year) & (df["startMonth"]== holidays[1]) & (df["startDay"]== holidays[0]), "isStateHolidays"] = True
            
    for holidays in state_holidays_year_dependent:
        df.loc[(df["startYear"]== holidays[2]) & (df["startMonth"]== holidays[1]) & (df["startDay"]== holidays[0]), "isStateHolidays"] = True
    df["isSchoolWorkingHours"] = df["startHour"].between(8,14, inclusive=True)
    df["isSchoolBreak"] = False
    school_breaks = [(8,45,55), (9, 40, 59), (10, 45, 55), (11, 40, 50), (12, 35, 45), (13, 30, 40)]
    for b in school_breaks:
        df.loc[(df["startHour"] == b[0]) & (df["startMinute"].between(b[1], b[2], inclusive=True)), "isSchoolWorkingHours"] = False
        df.loc[(df["startHour"] == b[0]) & (df["startMinute"].between(b[1], b[2], inclusive=True)), "isSchoolBreak"] = True
    df["isWeekend"] = df["startDayofweek"].between(5,6, inclusive=True)
    df.loc[(df["isWeekend"]), "isSchoolWorkingHours"] = False
    df.loc[(df["isSummerHolidays"]), "isSchoolWorkingHours"] = False
    df.loc[(df["isStateHolidays"]), "isSchoolWorkingHours"] = False
    df["isShortlyBeforeBreak"] = False
    before_time_mins = 10
    for b in school_breaks:
        df.loc[(df["startHour"] == b[0]) & (df["startMinute"].between(b[1]-before_time_mins, b[1], inclusive=True) & (df["isSchoolWorkingHours"])), "isShortlyBeforeBreak"] = True

    df["isMorning"] = False
    df.loc[df["startHour"].between(5,12, inclusive=True), "isMorning"] = True
    df["isAfternoon"] = False
    df.loc[df["startHour"].between(13,18, inclusive=True), "isAfternoon"] = True
    df["isEvening"] = False
    df.loc[df["startHour"].between(19,22, inclusive=True), "isEvening"] = True
    df["isNight"] = False
    df.loc[(df["startHour"] < 5) | (df["startHour"] > 22), "isNight"] = True
    
def get_cols_to_drop():
    return ["mean_per_task", "time", "std_per_task", "start", "response_time_sec", "edits", "executions"]

def get_ready_for_training(df):
    # drop unsolved tasks
    df_solved = df.dropna()
    y = df_solved["time"]
    to_drop = get_cols_to_drop()
    X = df_solved.drop(to_drop, axis=1, errors='ignore')
    return X,y

def create_student_task_df():
    return pd.DataFrame(columns=['student', 'task', 'mean_per_task', 'std_per_task', 'start', 'time', 'n_of_solved', 's_since_last_solved', 'solved', 'is_successful', \
    'is_in_school', 'is_before_break', 'is_holidays', 'is_morning', 'is_afternoon', 'is_evening', 'is_night'])

def create_student_task_data(df, student_item_df):
    prev_row = pd.DataFrame()
    for pair in df[["student", "item"]].values:
        current_row = df[(df["student"] == pair[0]) & (df["item"] == pair[1])]
        s_since_last_solved = 0
        if not prev_row.empty and prev_row["student"].values[0] == current_row["student"].values[0]: # it's not a new student
            s_since_last_solved = (current_row['start'].values[0]-prev_row['start'].values[0]) / np.timedelta64(1, 's')
        mean = current_row[["time"]].mean()[0]
        std = current_row[["time"]].std()[0]
        if math.isnan(std): std = 0
            
        if current_row["solved"].describe().values[2]:
            n_of_solved = current_row["solved"].describe().values[3] - (current_row["solved"].describe().values[1]-current_row["solved"].describe().values[3])
        else:
            n_of_solved = -current_row["solved"].describe().values[3] + (current_row["solved"].describe().values[1]-current_row["solved"].describe().values[3])
        if current_row["solved"].describe().values[1] == 1 and current_row["solved"].describe().values[2] == False:
            mean = 0
            std = 0
        student_item_df = student_item_df.append({
            'student': int(current_row["student"].values[0]), 
            'task': int(current_row["item"].values[0]),
            'time': current_row["time"].values[0],
            'start': current_row["start"].values[0], 
            'n_of_solved': int(n_of_solved),
            'mean_per_task': mean, 
            'std_per_task': std, 
            's_since_last_solved': s_since_last_solved}, ignore_index=True)
        prev_row = current_row
    return student_item_df

def create_student_data(df, student_item_df):
    for student in student_item_df["student"].values:
        current_s = df[df["student"] == student]

        success = current_s["solved"].mean()
        student_item_df.loc[student_item_df["student"] == student, "solved"] = success
        if success > 0.8: # arbitrary
            student_item_df.loc[student_item_df["student"] == student, "is_successful"] = True
        else:
            student_item_df.loc[student_item_df["student"] == student, "is_successful"] = False

        schoolboi = current_s["isSchoolWorkingHours"].mean()
        if schoolboi > 0.9: # arbitrary
            student_item_df.loc[student_item_df["student"] == student, "is_in_school"] = True
        else:
            student_item_df.loc[student_item_df["student"] == student, "is_in_school"] = False

        before_break = current_s["isShortlyBeforeBreak"].mean()
        if before_break > 0.2: # arbitrary
            student_item_df.loc[student_item_df["student"] == student, "is_before_break"] = True
        else:
            student_item_df.loc[student_item_df["student"] == student, "is_before_break"] = False

        holidays = current_s["isStateHolidays"].mean() + current_s["isSummerHolidays"].mean()
        if holidays > 0.2: # arbitrary  
            student_item_df.loc[student_item_df["student"] == student, "is_holidays"] = True
        else:
            student_item_df.loc[student_item_df["student"] == student, "is_holidays"] = False

        start_hour = current_s["startHour"].mean()
        if start_hour >= 5 and start_hour < 12: # arbitrary
            student_item_df.loc[student_item_df["student"] == student, "is_morning"] = True
        else:
            student_item_df.loc[student_item_df["student"] == student, "is_morning"] = False

        if start_hour >= 12 and start_hour < 18: # arbitrary
            student_item_df.loc[student_item_df["student"] == student, "is_afternoon"] = True
        else:
            student_item_df.loc[student_item_df["student"] == student, "is_afternoon"] = False

        if start_hour >= 18 and start_hour < 22: # arbitrary
            student_item_df.loc[student_item_df["student"] == student, "is_evening"] = True
        else:
            student_item_df.loc[student_item_df["student"] == student, "is_evening"] = False

        if start_hour >= 22 or start_hour < 5: # arbitrary
            student_item_df.loc[student_item_df["student"] == student, "is_night"] = True
        else:
            student_item_df.loc[student_item_df["student"] == student, "is_night"] = False

    return student_item_df

def get_feature_importances(X, y):
    rf = RandomForestRegressor(n_jobs=-1, oob_score=True)
    start_time = time.time()
    rf.fit(X, y)
    end_time = time.time() - start_time
    indices = np.argsort(rf.feature_importances_)[::-1]
    # Print the feature ranking
    print('Feature ranking:')
    for f in range(X.shape[1]):
        print(f"{f+1}. feature: '{X.columns[indices[f]]}' ({rf.feature_importances_[indices[f]]})")
    return rf, time

def feature_importances(coeffs, columns):
    coef_s = pd.Series(coeffs, index = columns)
    #sorted(coef)
    #matplotlib.rcParams['figure.figsize'] = (8.0, 10.0)
    coef_s.plot(kind = "barh")
    plt.title("Coefficients in the Linear Model")