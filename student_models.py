from robomission import *

class ItemAvgModelUserSolvedCount(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]     
        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen items
            'item_mean_times': solved_attempts.groupby('item').time.mean(),
            'student_percentage_solved': attempts.groupby("student").solved.mean(),
            'student_count_solved': attempts.groupby("student").solved.count()
        }
    
    def predict(self, attempt):
        item_mean_times = self.pparams['item_mean_times']
        global_mean_time = self.pparams['global_mean_time']
        if attempt["student"] in self.pparams['student_count_solved']:
          return item_mean_times.get(attempt['item'], default=global_mean_time) - 0.01*self.pparams['student_count_solved'][attempt["student"]]
        else:
            return item_mean_times.get(attempt['item'], default=global_mean_time)

    def debug(self):
        return self.pparams['student_count_solved']

class UserAvgModel(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved]     
        self.pparams = {
            'global_mean_time': solved_attempts.time.mean(),  # for unseen users
            'student_mean_times': solved_attempts.groupby('student').time.mean()
        }
    
    def predict(self, attempt):
        student_mean_times = self.pparams['student_mean_times']
        global_mean_time = self.pparams['global_mean_time']
        return student_mean_times.get(attempt['student'], default=global_mean_time)

class UserAvgModelHistory(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved] 

        grouped = solved_attempts.groupby("student")
        item_mean_times = solved_attempts.groupby('item').time.mean()
        #print(item_mean_times)
        global_mean_time = solved_attempts.time.mean()

        student_perf = {}
        i = 0
        #print("here")
        for key,group in grouped:
            #print("here 2")
            #print(key)
            #if group["item"] in item_mean_times:
            time = group["time"].values
            items = item_mean_times.get(group["item"]).values
            student_perf[key] = np.array(time - items).mean()
   
        self.pparams = {
            'global_mean_time': global_mean_time,  # for unseen users
            'item_mean_times': item_mean_times,
            'student_perf': student_perf,
        }
    
    def predict(self, attempt):
        global_mean_time = self.pparams['global_mean_time']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        if attempt['student'] in student_perf:
            #print(f"{item_mean_times.get(attempt['student'], default=global_mean_time) + student_perf[attempt['student']]} instd. of {item_mean_times.get(attempt['item'], default=global_mean_time)}")
            return item_mean_times.get(attempt['item'], default=global_mean_time) + student_perf[attempt['student']]
        else:
            return item_mean_times.get(attempt['item'], default=global_mean_time)

    def debug(self):
        return self.pparams['student_perf']

class UserAvgModelHistoryHandicapOnFirstTaskEver(StudentModel):
    """Predicts the item success rate estimated offline.
    """
    def fit(self, attempts):
        solved_attempts = attempts[attempts.solved] 

        grouped = solved_attempts.groupby("student")
        item_mean_times = solved_attempts.groupby('item').time.mean()
        #print(item_mean_times)
        global_mean_time = solved_attempts.time.mean()

        student_perf = {}
        i = 0
        #print("here")
        for key,group in grouped:
            #print("here 2")
            #print(key)
            #if group["item"] in item_mean_times:
            time = group["time"].values
            items = item_mean_times.get(group["item"]).values
            diffs = np.array(time - items)
            if (time[0] - items[0])/2 > 0:
                diff_term = ((time[0] - items[0])/2)
                diffs = np.append(diffs, diff_term*1.25)
            else:
                diff_term = time[0] - items[0] + ((time[0] - items[0])/2)
                diffs = np.append(diffs, diff_term*0.75)
            #if key == 769 or key == 5 or key == 971:
                #print(key)
                #print(f"{diffs[1:].mean()} instd. of {np.array(time - items).mean()}")
            student_perf[key] = diffs[1:].mean()
   
        self.pparams = {
            'global_mean_time': global_mean_time,  # for unseen users
            'item_mean_times': item_mean_times,
            'student_perf': student_perf
        }
    
    def predict(self, attempt):
        global_mean_time = self.pparams['global_mean_time']
        item_mean_times = self.pparams['item_mean_times']
        student_perf = self.pparams['student_perf']
        if attempt['student'] in student_perf:
            #print(f"{item_mean_times.get(attempt['student'], default=global_mean_time) + student_perf[attempt['student']]} instd. of {item_mean_times.get(attempt['item'], default=global_mean_time)}")
            return item_mean_times.get(attempt['item'], default=global_mean_time) + student_perf[attempt['student']]
        else:
            return item_mean_times.get(attempt['item'], default=global_mean_time)

    def debug(self):
        return self.pparams['student_perf']